# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression.
Use the insertion algorithm"""

import sys

# Ordered list of image formats, from lower to higher insertion
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    resultado = ""
    if format1 < format2:
        resultado = True
    else:
        resultado = False
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """

    return resultado


def find_lower_pos(formats: list, pivot: int):
    menor = pivot
    for i in range(pivot, len(formats)):
        if lower_than(formats[i], formats[menor]):
            menor = i
    """Sort the pivot format, by exchanging it with the format
    on its left, until it gets ordered.
    """

    return menor


def sort_formats(formats: list) -> list:
    for pivot_pos in range(len(formats)):
        lower_pos: int = find_lower_pos(formats, pivot_pos)
        if lower_pos != pivot_pos:
            (formats[pivot_pos], formats[lower_pos]) = \
                (formats[lower_pos], formats[pivot_pos])
    return formats


"""Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered. Use the insertion algorithm"""


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()
